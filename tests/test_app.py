import asyncio
import unittest

from async_collie import App, Applet
from async_collie.common import Events, Status
from async_collie.resources import Provides, Requires


class Counter(asyncio.Lock):
    def __init__(self, start_value, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.value = start_value

    def inc(self):
        if not self.locked():
            raise RuntimeError("The resource was not locked before access")
        self.value += 1


class TestApplet(Applet):

    app_counter: Counter = Requires()

    def __init__(self, name: str, critical=False, **kwargs) -> None:
        super().__init__(name, loop=True, critical=critical, **kwargs)
        self.run_counter = Counter(0)

    async def main(self):
        await asyncio.sleep(0.01)
        async with self.run_counter:
            self.run_counter.inc()
        async with self.app_counter:
            self.app_counter.inc()
        return self.run_counter.value


class HaltApplet(Applet):
    async def main(self):
        await asyncio.sleep(0.1)
        await self.event(Events.HALT)
        return "DONE"


class TestApp(App):

    app_counter: Counter = Provides()

    async def create_provides(self):
        await super().create_provides()
        self.app_counter = Counter(0)

    async def create_applets(self):
        self.add_applet(TestApplet("app1"))
        self.add_applet(TestApplet("app2"))
        self.add_applet(HaltApplet("halt"))


class TestCriticalityApp(App):

    app_counter: Counter = Provides()

    async def create_provides(self):
        await super().create_provides()
        self.app_counter = Counter(0)

    async def create_applets(self):
        self.add_applet(TestApplet("critical", critical=True))
        self.add_applet(TestApplet("non-critical"))


class TestDepsApp(App):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.start_order = []
        self.stop_order = []

    app_counter: Counter = Provides()

    async def create_provides(self):
        await super().create_provides()
        self.app_counter = Counter(0)

    async def create_applets(self):
        # App order shuffled to make a coincidental false posative unlikely
        self.add_applet(TestApplet("app1", depends_on=["app2"]))
        self.add_applet(TestApplet("app3", depends_on=["app4"]))
        self.add_applet(TestApplet("app5", depends_on=["app6"]))
        self.add_applet(TestApplet("app2", depends_on=["app3"]))
        self.add_applet(TestApplet("app6"))
        self.add_applet(TestApplet("app4", depends_on=["app5"]))
        self.add_applet(HaltApplet("halt"))

    async def applet_start(self, applet: Applet) -> None:
        await super().applet_start(applet)
        self.start_order.append(applet.name)

    async def applet_stop(self, applet: Applet) -> None:
        await super().applet_stop(applet)
        self.stop_order.append(applet.name)


class StopAfterApplet(Applet):
    def __init__(self, delay, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.delay = delay

    async def main(self):
        await asyncio.sleep(self.delay)
        return "DONE"


class StopAfterApp(App):
    async def create_applets(self):
        # App order shuffled to make a coincidental false posative unlikely
        self.add_applet(StopAfterApplet(0.1, "app1"))
        self.add_applet(StopAfterApplet(0.2, "app2"))
        self.add_applet(StopAfterApplet(0.3, "app3"))


class AppTestCases(unittest.TestCase):
    def test_app_terminate(self):
        app = TestApp()
        app.run()

        run_counter = 0
        for applet in app.applets.values():
            if not isinstance(applet, TestApplet):
                continue
            self.assertGreater(applet.run_counter.value, 1)
            run_counter = applet.run_counter.value
        self.assertGreater(app.app_counter.value, 1)
        self.assertAlmostEqual(run_counter / app.app_counter.value, 0.5, 1)

    def test_non_critical_applet(self):
        app = TestCriticalityApp()

        async def main():
            app_task = asyncio.create_task(app.main())
            await asyncio.sleep(0.01)
            self.assertEqual(app.status(), Status.RUNNING)
            app.applets["non-critical"].kill()
            await asyncio.sleep(0.01)
            self.assertEqual(app.status(), Status.RUNNING)

            app_task.cancel()
            try:
                await app_task
            except asyncio.CancelledError:
                pass

        asyncio.run(main())

    def test_critical_applet(self):
        app = TestCriticalityApp()

        async def main():
            app_task = asyncio.create_task(app.main())
            await asyncio.sleep(0.01)
            self.assertEqual(app.status(), Status.RUNNING)
            app.applets["critical"].kill()
            await asyncio.sleep(0.01)
            self.assertEqual(app.status(), Status.STOPPED)

            app_task.cancel()
            try:
                await app_task
            except asyncio.CancelledError:
                pass

        asyncio.run(main())

    def test_return_values(self):
        app = TestApp()
        ret_val = app.run()

        self.assertGreater(ret_val["app1"], 1)
        self.assertGreater(ret_val["app2"], 1)
        self.assertEqual(ret_val["halt"], "DONE")

    def test_app_deps(self):
        """Tests a system where
            1 -> 2 -> 3 -> 4 -> 5 -> 6
        and
            halt is an isolate
        """
        app = TestDepsApp()
        app.run()

        self.assertListEqual(
            app.start_order, ["app6", "halt", "app5", "app4", "app3", "app2", "app1"]
        )
        self.assertListEqual(
            app.stop_order, ["halt", "app1", "app2", "app3", "app4", "app5", "app6"]
        )

    def test_stop_app(self):
        app = StopAfterApp()
        app.run()
