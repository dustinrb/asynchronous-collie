import unittest
from typing import Any, List

from async_collie.resources import (
    Provides,
    Requires,
    ResourceMixin,
    ResourceNotAllocated,
)


class ResourcedClass(ResourceMixin):

    a: int = Requires()
    a2 = Requires()
    b: str = Provides()
    c: List[int] = Requires()  # Cannot be used with instance checks

    def __init__(self, b=None) -> None:
        if b:
            self.b = b


class ResourcesTestCases(unittest.TestCase):
    def test_get_provides(self):
        # Should error out if not all providers are supplied
        with self.assertRaises(ResourceNotAllocated):
            ResourcedClass()._get_provides()

        resource = ResourcedClass(b="string")
        self.assertDictEqual(resource._get_provides(), {"b": "string"})

    def test_get_required_list(self):
        self.assertListEqual(
            ResourcedClass()._get_required_attrs(),
            [("a", int), ("a2", Any), ("c", List[int])],
        )

    def test_meet_requirements(self):
        resource = ResourcedClass(b="string")

        with self.assertRaises(TypeError):
            resource._meet_requirements(a=1.0, a2="any", c=[1, 2])

        resource._meet_requirements(a=1, a2="any", c=[1, 2])

        self.assertEqual(resource.a, 1)
        self.assertEqual(resource.a2, "any")
