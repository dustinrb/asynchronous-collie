import asyncio
import unittest

from async_collie import Applet, Events


class TestApplet(Applet):
    def __init__(self, name: str, loop=False) -> None:
        super().__init__(name, loop=loop)
        self.run_counter = 0

    async def main(self):
        self.run_counter += 1
        await asyncio.sleep(0.01)  # Pause long enough to let other tasks do work

    async def main_crash(self, exc: Exception):
        raise Exception("There was an exception") from exc


class CrashApplet(Applet):
    def __init__(self, name: str) -> None:
        super().__init__(name, loop=True)

    async def main(self):
        raise Exception("This is expected")

    async def tearDown(self):
        return await super().tearDown()


class AppletTestCases(unittest.TestCase):
    def test_main(self):
        async def main():
            app = TestApplet("test_applet")
            app_status_queue = asyncio.Queue()
            app._meet_requirements(
                app_soft_stop=asyncio.Event(),
                app_hard_stop=asyncio.Event(),
                app_status_queue=app_status_queue,
            )
            await app.applet_life_cycle()

            events = []
            while not app_status_queue.empty():
                events.append(app_status_queue.get_nowait()[0])

            self.assertListEqual(events, [Events.START, Events.STOP])
            self.assertEqual(app.run_counter, 1)

        asyncio.run(main())

    def test_terminate(self):
        async def main():
            app = TestApplet("test_applet", True)
            app_status_queue = asyncio.Queue()
            app._meet_requirements(
                app_soft_stop=asyncio.Event(),
                app_hard_stop=asyncio.Event(),
                app_status_queue=app_status_queue,
            )
            await app.start_applet_task()
            await asyncio.sleep(0.03)
            app.terminate()
            await app.wait()

            events = []
            while not app_status_queue.empty():
                events.append(app_status_queue.get_nowait()[0])

            self.assertListEqual(events, [Events.START, Events.STOP])
            self.assertGreater(app.run_counter, 1)

        asyncio.run(main())

    def test_kill(self):
        async def main():
            app = TestApplet("test_applet", True)
            app_status_queue = asyncio.Queue()
            app._meet_requirements(
                app_soft_stop=asyncio.Event(),
                app_hard_stop=asyncio.Event(),
                app_status_queue=app_status_queue,
            )
            await app.start_applet_task()
            await asyncio.sleep(0.03)
            app.kill()
            await app.wait()

            events = []
            while not app_status_queue.empty():
                events.append(app_status_queue.get_nowait()[0])

            self.assertListEqual(events, [Events.START, Events.STOP])
            self.assertGreater(app.run_counter, 1)

        asyncio.run(main())

    def test_exception(self):
        async def main():
            app = CrashApplet("test_applet")
            app_status_queue = asyncio.Queue()
            app._meet_requirements(
                app_soft_stop=asyncio.Event(),
                app_hard_stop=asyncio.Event(),
                app_status_queue=app_status_queue,
            )
            await app.applet_life_cycle()

            events = []
            while not app_status_queue.empty():
                events.append(app_status_queue.get_nowait()[0])
            self.assertListEqual(events, [Events.START, Events.STOP])
            self.assertEqual(str(app.exception()), "This is expected")

        asyncio.run(main())
