import abc
import asyncio
import logging
from typing import Any, Iterable, List, Union

from async_collie.common import Events, ExitMainLoop, Status
from async_collie.resources import Requires, ResourceMixin

log = logging.getLogger(__name__)


class Applet(ResourceMixin, abc.ABC):
    """Small app for running long-lived code in the asyncio loop"""

    app_status_queue: asyncio.Queue = Requires()
    app_soft_stop: asyncio.Event = Requires()
    app_hard_stop: asyncio.Event = Requires()

    # Create references to control structures
    applet_task: asyncio.Task = None
    main_loop_task: asyncio.Task = None

    configured_event: asyncio.Event = None
    start_event: asyncio.Event = None
    soft_stop_event: asyncio.Event = None
    hard_stop_event: asyncio.Event = None

    # Code for handling dependencies
    node_id: int = None

    def __init__(
        self,
        name: str,
        loop=False,
        critical=False,
        depends_on: List[Iterable[str]] = None,
    ) -> None:
        """Configure the applet

        Args:
            name (str): Name of the applet determined by the app
            loop (bool): Flag. If true the :meth:`.main` method is called until
                an exception occures or ExitMainLoop is raised.
            critical (bool): Flag. If true, the App should exit if this applet
                returns.
        """
        self.name = name
        self.loop = loop
        self.critical = critical
        self.depends_on = depends_on or []

    async def async_init(self, force=False):
        """Create all asynchronious attributes (including provides).

        Args:
          force (bool): Run configuration regardless of it being run before.
            USE WITH CAUTION

        ..note:: This function is idempotent so it can be called repeatedly unleas
        """
        if (not force) and (not self.configured_event is None):
            return

        self.configured_event = asyncio.Event()
        self.start_event = asyncio.Event()
        self.soft_stop_event = asyncio.Event()
        self.hard_stop_event = asyncio.Event()
        self.main_loop_stop_event = asyncio.Event()
        await self.create_provides()

        self.configured_event.set()

    # ===========================================================================
    #                            USER FACEING METHODS
    # ===========================================================================

    @abc.abstractmethod
    async def main(self):
        """User-defined function which will be run until soft_stop is enabled"""
        pass

    # ===========================================================================
    #                           STATUS
    # ===========================================================================

    def status(self) -> Status:
        if self.exception():
            return Status.CRASHED

        if not self.applet_task_running():
            if self.is_shutting_down():
                return Status.STOPPED
            return Status.PENDING

        if self.is_shutting_down():
            return Status.STOPPING
        return Status.RUNNING

    def main_loop_task_running(self) -> bool:
        if self.main_loop_task and not self.main_loop_task.done():
            return True
        return False

    def applet_task_running(self) -> bool:
        if self.applet_task and not self.applet_task.done():
            return True
        return False

    def is_shutting_down(self) -> bool:
        if any(
            [
                self.soft_stop_event and self.soft_stop_event.is_set(),
                self.hard_stop_event and self.hard_stop_event.is_set(),
                self.main_loop_stop_event and self.main_loop_stop_event.is_set(),
            ]
        ):
            return True
        return False

    def result(self) -> Any:
        if self.main_loop_task and self.main_loop_task.done():
            return self.main_loop_task.result()
        return None

    def exception(self) -> Union[None, BaseException]:
        if self.applet_task and self.applet_task.done():
            if self.main_loop_task.exception():
                return self.applet_task.exception()

        if self.main_loop_task and self.main_loop_task.done():
            if self.main_loop_task.exception():
                return self.main_loop_task.exception()
        return None

    # ===========================================================================
    #                            Resource Management
    # ===========================================================================

    async def create_provides(self) -> None:
        pass

    async def cleanup_provides(self) -> None:
        pass

    # ===========================================================================
    #                            INTERNAL LOOP AND SIGNAL MANAGEMENT
    # ===========================================================================

    async def event(self, event: Events) -> None:
        await self.app_status_queue.put((event, self.name))

    # ===========================================================================
    #                            EXTERNAL LOOP MANAGMENT
    # ===========================================================================

    async def start_applet_task(self) -> asyncio.Task:
        """Returns a running task for the current applet

        Returns:
            (asyncio.Task): Task of the self.run() coroutine
        """
        if not self.requirements_met():
            raise ValueError("Resource requirements not met.")

        # This should only be run once
        if self.applet_task_running():
            return self.applet_task

        await self.async_init()  # Idempotent so calling twice is not an issue
        self.applet_task = asyncio.create_task(self.applet_life_cycle())
        return self.applet_task

    async def applet_life_cycle(self):
        """Runs the applet lifecycle"""
        await self.async_init()  # Idempotent
        try:
            await self.configure()
            await self.start_main_loop()
            await self.wait_main_loop()
            await self.soft_stop()
        finally:
            await self.hard_stop()

    async def wait(self):
        await self.applet_task

    async def configure(self) -> None:
        """Steps needed to make the mian loop operational

        .. note:: Shared resources should not be initilized here. That should
          happen in the :meth:`.create_provides` method.
        """
        pass

    async def cleanup(self):
        await self.cleanup_provides()

    def terminate(self):
        self.soft_stop_event.set()

    def kill(self):
        self.hard_stop_event.set()

    # ===========================================================================
    #                            Main Loop Management
    # ===========================================================================

    async def start_main_loop(self):
        # Small looping coroutine to allow infinite restarts
        self.main_loop_task = asyncio.create_task(self.main_loop())
        await self.event(Events.START)

    async def wait_main_loop(self):
        soft_stop_task = asyncio.create_task(self.soft_stop_event.wait())
        hard_stop_task = asyncio.create_task(self.hard_stop_event.wait())
        return await asyncio.wait(
            [soft_stop_task, hard_stop_task, self.main_loop_task],
            return_when=asyncio.FIRST_COMPLETED,
        )

    async def soft_stop(self):
        if self.hard_stop_event.is_set():
            return  # Do nothing if we are told to come down fast
        if self.exception():
            return  # Loop has crashed so we shouldn't tear down

        # Now let the other parts of the app know it's time to shut down
        self.soft_stop_event.set()

    async def hard_stop(self):
        self.soft_stop_event.set()
        self.hard_stop_event.set()

        # Tear down the main task
        if self.main_loop_task and not self.main_loop_task.done():
            # Handle running loop
            self.main_loop_task.cancel()
            try:
                await self.main_loop_task
            except asyncio.CancelledError:
                pass

        await self.cleanup()
        await self.event(Events.STOP)

    async def main_loop(self):
        """Loop to keep the :method:`.main` running"""
        self.start_event.set()
        result_val = None
        while not self.main_loop_stop_event.is_set():
            try:
                result_val = await self.main()
            except asyncio.CancelledError:
                self.main_loop_stop_event.set()
            except ExitMainLoop:
                self.main_loop_stop_event.set()
            if not self.loop:
                self.main_loop_stop_event.set()
        return result_val

    async def main_crash(self, exc: Exception):
        # The loop will restart automatically. Be sure to clean up everything
        # befor continueing
        self.soft_stop_event.set()
        await self.cleanup()
        self.hard_stop_event.set()
        await self.event(Events.CRASH)
        self.main_loop_stop_event.set()
