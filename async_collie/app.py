import abc
import asyncio
import signal
from asyncio.tasks import Task
from functools import partial
from itertools import combinations
from typing import Any, Dict, List, Set, Tuple

import networkx as nx

from async_collie import Applet
from async_collie.common import Events, Status
from async_collie.resources import Provides, Resource, ResourceMixin

SIGNAL_COUNTER = 0


import logging

log = logging.getLogger(__name__)


class AppException(Exception):
    pass


class UnknownEventError(AppException):
    pass


class AppletCrash(AppException):
    pass


class AppStartupError(AppException):
    pass


class App(ResourceMixin, abc.ABC):

    app_status_queue: asyncio.Queue = Provides()
    app_configured: asyncio.Event = Provides()
    app_soft_stop: asyncio.Event = Provides()
    app_hard_stop: asyncio.Event = Provides()

    signals = (
        (signal.SIGINT, "Interrupted"),
        (signal.SIGTERM, "Terminated"),
        (signal.SIGUSR1, "User 1"),
        (signal.SIGUSR2, "User 2"),
    )

    node_id = 0

    def __init__(self, hard_stop_timeout=10) -> None:
        self.applets: Dict[str, Applet] = {}
        self.hard_stop_timeout = hard_stop_timeout

        self.status_task: asyncio.Task = None

    def is_configured(self) -> bool:
        if self.app_configured is None:
            return False
        elif isinstance(self.app_configured, Resource):
            return False
        elif self.app_configured.is_set():
            return True
        else:
            return False

    async def async_init(self, force=False) -> None:
        if (not force) and self.is_configured():
            return

        self.app_status_queue = asyncio.Queue()
        self.app_configured = asyncio.Event()
        self.app_soft_stop = asyncio.Event()
        self.app_hard_stop = asyncio.Event()

        await self.create_provides()

        await self.create_applets()
        for applet in self.applets.values():
            await applet.async_init(force=force)

        app_provides = self._get_provides()
        self._meet_requirements(**app_provides)

        self.make_dep_list()

        self.app_configured.set()

    def make_dep_list(self) -> None:
        """Create dependency graph base on `depends_on` statements

        TODO: Do this based on Resources (requires/provides).
        """
        G = nx.DiGraph()
        for applet in self.applets.values():
            G.add_node(applet.name)
            # Add dependencies
            for dep_name in applet.depends_on:
                G.add_edge(applet.name, dep_name)

        self.dep_graph = G  # Store this for good measure

    def applet_startable(self, applet_name: str):
        applet = self.applets[applet_name]
        if applet.status() == Status.RUNNING:
            return False

        startable = True
        for _, n in self.dep_graph.out_edges(applet_name):
            child_applet = self.applets[n]
            if child_applet.status() != Status.RUNNING:
                startable = False
        return startable

    def applet_stoppable(self, applet_name: str):
        """
        An applet is stoppable iff all it's child applets (applets it depends
        on) are not running
        """
        applet = self.applets[applet_name]
        if applet.status() != Status.RUNNING:
            return False

        stoppable = True
        for n, _ in self.dep_graph.in_edges(applet_name):
            child_applet = self.applets[n]
            if child_applet.status() in (Status.RUNNING, Status.STOPPING):
                stoppable = False
        return stoppable

    # ===========================================================================
    #                            Resource Management
    # ===========================================================================

    def _get_provides(self) -> Dict[str, Any]:
        self_provides = super()._get_provides()
        applets_provide = [a._get_provides() for a in self.applets.values()]

        # Make sure we are not defining resources twice
        for a, b in combinations(applets_provide + [self_provides], 2):
            overlap = set.intersection(set(a.keys()), set(b.keys()))

            if overlap:
                raise ValueError(f"The key(s) {overlap} were defined multiple times")

        for ap in applets_provide:
            self_provides.update(ap)
        return self_provides

    def _meet_requirements(self, **providers: Tuple[str, Any]) -> None:
        super()._meet_requirements(**providers)
        for ap in self.applets.values():
            ap._meet_requirements(**providers)

    async def create_provides(self) -> None:
        pass

    async def cleanup_provides(self) -> None:
        pass

    # ===========================================================================
    #                            Applet Management
    # ===========================================================================

    def add_applet(self, applet: Applet) -> None:
        self.applets[applet.name] = applet

    @abc.abstractmethod
    async def create_applets(self) -> None:
        pass

    def applet_list(self, critical=None) -> List[Applet]:
        applets = self.applets.values()
        if not critical is None:
            applets = filter(lambda x: x.critical, applets)
        return applets

    def applet_task_list(self, critical=None) -> List[asyncio.Task]:
        return [a.applet_task for a in self.applet_list(critical)]

    async def start_applets(self) -> None:
        root_tasks = []
        for n, d in self.dep_graph.in_degree:
            if d == 0:  # Nodes which have no dependencies
                root_tasks.append(self.applets[n])
        await self.start_startable_applets()
        await asyncio.wait(
            [asyncio.create_task(n.start_event.wait()) for n in root_tasks]
        )

    async def start_startable_applets(self) -> None:
        if self.app_soft_stop.is_set():
            return
        for applet in self.applets.values():
            if self.applet_startable(applet.name):
                await applet.start_applet_task()

    async def stop_applets(self, done: Set[Task], pending: Set[Task]) -> None:
        self.app_soft_stop.set()
        if self.app_hard_stop.is_set():
            return

        root_tasks: List[Applet] = []
        for n, d in self.dep_graph.out_degree:
            if d == 0:  # Nodes which have no dependencies
                root_tasks.append(self.applets[n])
        await self.stop_stoppable_applets()
        await asyncio.wait(
            [asyncio.create_task(n.hard_stop_event.wait()) for n in root_tasks]
        )

    async def stop_stoppable_applets(self):
        for applet in self.applets.values():
            if self.applet_stoppable(applet.name):
                applet.terminate()

    # ===========================================================================
    #                            App Management
    # ===========================================================================

    def run(self) -> None:
        return asyncio.run(self.main())

    def status(self) -> Status:
        if not self.app_soft_stop:
            return Status.PENDING

        applet_status = {a.status() for a in self.applet_list(critical=True)}
        if Status.CRASHED in applet_status:
            return Status.CRASHED
        elif self.app_soft_stop.is_set() and not self.status_task.done():
            return Status.STOPPING
        elif self.app_soft_stop.is_set() and self.status_task.done():
            return Status.STOPPED
        else:
            return Status.RUNNING

    async def configure(self) -> None:
        pass

    async def main(self) -> Dict[str, Any]:
        # Startup tasks and error handling
        try:
            # App configuration
            await self.async_init()
            self.setup_signals()

            # Handle applet status events
            self.status_task = asyncio.create_task(
                self.status_listener(self.app_status_queue)
            )
        except Exception as e:
            await self.cleanup()
            raise AppStartupError("App failed to start") from e

        # Run applets
        await self.start_applets()
        if not self.applet_task_list():
            raise RuntimeError("No applet tasks started")

        try:
            done, pending = await asyncio.wait(
                self.applet_task_list(critical=True)
                + [
                    self.status_task,
                    asyncio.create_task(self.app_soft_stop.wait()),
                    asyncio.create_task(self.app_hard_stop.wait()),
                ],
                return_when=asyncio.FIRST_COMPLETED,
            )
            log.debug("STARTING SHUTDOWN PROCESS")
            await self.stop_applets(done, pending)
            log.debug("APPLETS ARE SHUTDOWN")
            return {
                applet_name: applet.result()
                for applet_name, applet in self.applets.items()
            }
        finally:
            await self.cleanup()

    async def cleanup(self) -> None:
        for applet in self.applets.values():
            applet.kill()

        if self.status_task and not self.status_task.done():
            await self.app_status_queue.join()
            self.status_task.cancel()
            try:
                await self.status_task
            except asyncio.CancelledError:
                pass
        self.stop_signals()
        await self.cleanup_provides()

    # ===========================================================================
    #                            Event handling
    # ===========================================================================

    async def status_listener(self, app_status_queue: asyncio.Queue) -> None:
        while not self.app_hard_stop.is_set():
            event, applet_name = await app_status_queue.get()
            applet = self.applets[applet_name]

            if event == Events.START:
                await self.applet_start(applet)
            elif event == Events.STOP:
                await self.applet_stop(applet)
            elif event == Events.CRASH:
                await self.applet_crash(applet)
            elif event == Events.HALT:
                await self.applet_halt(applet)
            else:
                raise UnknownEventError(f"The event '{event}' is unknown.")
            app_status_queue.task_done()

    async def applet_start(self, applet: Applet) -> None:
        await self.start_startable_applets()

    async def applet_stop(self, applet: Applet) -> None:
        # Stop the app if all applests are stopped.
        applet_states = {a.status() for a in self.applets.values()}
        if not applet_states.intersection({Status.RUNNING, Status.STARTING}):
            self.app_soft_stop.set()

        # Stop all stoppable applets
        if self.app_soft_stop.is_set() or applet.critical:
            self.app_soft_stop.set()
            await self.stop_stoppable_applets()

    async def applet_crash(self, applet: Applet) -> None:
        log.error(f"Applet '{applet.name}' crashed.")
        # raise AppletCrash(f"Applet '{applet.name}' crashed.")

    async def applet_halt(self, applet: Applet) -> None:
        self.app_soft_stop.set()

    # ===========================================================================
    #                            OS Signal Mangement
    # ===========================================================================

    def setup_signals(self) -> None:
        loop = asyncio.get_running_loop()
        for sig, sig_name in self.signals:
            loop.add_signal_handler(sig, partial(self.signal_handler, sig, sig_name))

    def stop_signals(self) -> None:
        """Sets all signals to ignore since we will be exiting anyway"""
        loop = asyncio.get_running_loop()
        for sig, _ in self.signals:
            loop.remove_signal_handler(sig)

    def signal_handler(self, sig, sig_name) -> None:
        log.warning(f"Recieved signal: {sig_name}")
        global SIGNAL_COUNTER
        if SIGNAL_COUNTER >= 0:
            self.app_soft_stop.set()
        elif SIGNAL_COUNTER >= 1:
            self.app_hard_stop.set()
        elif SIGNAL_COUNTER >= 3:
            raise InterruptedError("The app was forced down.")
        SIGNAL_COUNTER += 1
