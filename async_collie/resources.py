from inspect import getmembers
from typing import Any, Dict, List, Tuple, Type, get_type_hints


class ResourceNotAllocated(Exception):
    pass


class Resource:
    pass


class Requires(Resource):
    pass


class Provides(Resource):
    pass


class ResourceMixin:
    def _get_provides(self) -> Dict[str, Any]:
        """Validates the instance in search of resources

        Raises:
            ResourceNotAllocated: Raise if the instance has not defined a
                resource it claims to provide
            TypeError: Riased if the provided resource is not the proper
                type

        Returns:
            Dict[str, Any]: The resources this instance provides
        """
        provides = {}
        types = get_type_hints(self.__class__)
        for name, class_def in getmembers(
            self.__class__, lambda x: isinstance(x, Provides)
        ):
            var = getattr(self, name)
            if isinstance(var, Resource):
                raise ResourceNotAllocated(f"`{name}` has not been initilized.")

            if not instance_of(var, types.get(name, Any)):
                raise TypeError(f"Resource `{name}` must be a {types[name]}")
            provides[name] = var

        return provides

    def _get_required_attrs(self) -> List[Tuple[str, Type]]:
        """Returns a list of name/type tuples for the resources required by
        this class

        Returns:
            List[Tuple[str, Type]]: The resources required by this class
        """
        types = get_type_hints(self.__class__)
        return [
            (name, types.get(name, Any))
            for name, _ in getmembers(self.__class__, lambda x: isinstance(x, Requires))
        ]

    def _meet_requirements(self, **providers: Tuple[str, Any]) -> None:
        """Assigns resources to their proper variables

        Args:
            **providers (Tuple[str, Any]): Dictionary provided resources

        Raises:
            ResourceNotAllocated: The dict does not provide the required
               resource
            TypeError: If the provided resource is not the required type
        """
        requires = self._get_required_attrs()
        for (
            req,
            t,
        ) in requires:
            try:
                var = providers[req]
            except KeyError:
                raise ResourceNotAllocated(f"The requirement `{req}` was not provided")

            if not instance_of(var, t):
                raise TypeError(
                    f"{type(var)} given when {t} was expected for "
                    f"requirement '{req}'."
                )

            setattr(self, req, var)
        self._requirements_met = True

    def requirements_met(self):
        return hasattr(self, "_requirements_met")


def instance_of(var: Any, t: Type):
    if t == Any:
        return True

    try:
        return isinstance(var, t)
    except TypeError:
        # For "Subscripted generics"
        return True
