import enum


class Events(enum.Enum):
    START = enum.auto()
    STOP = enum.auto()
    CRASH = enum.auto()
    HALT = enum.auto()


class Status(enum.Enum):
    PENDING = enum.auto()
    STARTING = enum.auto()
    RUNNING = enum.auto()
    STOPPED = enum.auto()
    STOPPING = enum.auto()
    CRASHED = enum.auto()


class MustExistInLoop(Exception):
    pass


class ExitMainLoop(Exception):
    pass
