import asyncio
from datetime import datetime, timedelta
from typing import Coroutine


class DeadmanSwitch:
    def __init__(self, timeout: int) -> None:
        self.timeout = timedelta(0, timeout, 0)
        self.timer_start = None

    def press(self):
        self.timer_start = None

    def check(self, reference_time=None):
        if self.timer_start is None:
            self.timer_start = datetime.now()
            return
        if reference_time is None:
            reference_time = datetime.now()
        if reference_time > self.timer_start + self.timeout:
            raise TimeoutError("Timeout has been reach")

    @staticmethod
    async def run_with_timeout(cr: Coroutine, timeout: float):
        cr_task = asyncio.create_task(cr)
        timeout_task = asyncio.create_task(asyncio.sleep(timeout))
        await asyncio.wait([cr_task, timeout_task], return_when=asyncio.FIRST_COMPLETED)

        # Cleanup the timer
        if timeout_task.done():
            raise TimeoutError("Timeout has reached")
        else:
            timeout_task.cancel()
            try:
                await timeout_task
            except asyncio.CancelledError:
                pass

        # Return the result or raise the exception
        return cr_task.result
