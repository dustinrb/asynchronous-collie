from setuptools import find_packages, setup

requirements = ["networkx"]

doc_requirements = ["sphinx >= 4", "sphinx-autodoc-typehints >= 1.12", "esbonio"]

dev_requirements = []

setup(
    name="Asynchronous Collie",
    version="0.0.1",
    python_requires=">=3.7",
    packages=find_packages(where=".", include=["async_collie*"], exclude=["tests*"]),
    long_description=open("README.md").read(),
    install_requires=requirements,
    include_package_data=True,
    extras_require={
        "dev": dev_requirements + doc_requirements,
        "doc": doc_requirements,
    },
    test_suite="tests",
)
